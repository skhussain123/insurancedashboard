export const environment = {
  production: true,
  apiURL: 'https://mobileuat.mashreqbank.com/mashreqNeoWebApp/MashreqInsurance/Dashboard/',
  isMockEnabled: true // You have to switch this, when your real back-end is done
};
