import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { HttpUtilsService } from '../core/services/http-utils.service';
import { QueryParamsModel } from '../core/models/query-params.model';
import { QueryResultsModel } from '../core/models/query-results.model';
import { environment } from '../../environments/environment';
import { map, catchError, tap } from 'rxjs/operators';

const API_DASHBOARD_URL = environment.apiURL + 'api/dashboard';

@Injectable({
	providedIn: 'root'
})
export class DashboardService {

	lastFilter$: BehaviorSubject<QueryParamsModel> = new BehaviorSubject(new QueryParamsModel('', 'asc', '', 0, 10));

	constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

	// Server should return filtered/sorted result
	findFunnelRecords(): Observable<any> {
		// Note: Add headers if needed (tokens/bearer)
		const httpHeaders = this.httpUtils.getHTTPHeaders();
		// const httpParams = this.httpUtils.getFindHTTPParams(queryParams);
		// let endpointUrl = `${API_DASHBOARD_URL}/funnel`;
		let endpointUrl = `https://mobileuat.mashreqbank.com/mashreqNeoWebApp/MashreqInsurance/Dashboard/api/Dashboard`;
		return this.http.get(endpointUrl, { headers: httpHeaders })
			.pipe(
				tap(data => console.log("Anlagenstatus Daten:", data)),
				catchError(this.handleError('getData', [])),
			);
	}

	handleError(method: string, obj: any): any {
		console.log('Error in ' + method)
		return obj;
	}
}
