import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'm-funnel-chart',
  templateUrl: './funnel-chart.component.html',
  styleUrls: ['./funnel-chart.component.scss']
})
export class FunnelChartComponent implements OnInit {
  data = [
    {
      "level": 1,
      "count": 5000
    },
    {
      "level": 2,
      "count": 10000
    },
    {
      "level": 3,
      "count": 10000
    },
    {
      "level": 4,
      "count": 10000
    }
  ]
  constructor() { }

  ngOnInit() {
    this.backgroundChange();
  }

  countHeight(specificObj: { level: number, count: number }, data: { level: number, count: number }[], funnel: HTMLElement): string {
    let funnelHeight = funnel.clientHeight
    let countSum = 0;
    let specificObjCount = specificObj.count
    data.forEach((obj) => {
      countSum += obj.count
    })
    let heightOfSpecificLevel = specificObjCount * 100 / countSum;
    console.log('heightOfSpecificLevel', heightOfSpecificLevel);
    let heightInPercent = heightOfSpecificLevel + '%';
    return heightInPercent
  }
  backgroundChange() {
    this.data.forEach(obj => {
      obj['color'] = "#" + ((1 << 24) * Math.random() | 0).toString(16)
    });
  }
  trackByFn(index, item) {
    return index;
  }
}
