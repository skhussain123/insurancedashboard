import { Router } from '@angular/router';
import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { LayoutConfigService } from '../../../../core/services/layout-config.service';
import { SubheaderService } from '../../../../core/services/layout/subheader.service';
import { MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import { QueryResultsModel } from '../../../../core/models/query-results.model';
import { QueryParamsModel } from '../../../../core/models/query-params.model';
import { FunnelResultModel } from '../../../../models/funnel.model';
import { FunnelDataSource } from '../../../../core/models/data-sources/funnel.datasource';
import { DashboardService } from '../../../../services/dashboard.service';

@Component({
	selector: 'm-dashboard',
	templateUrl: './dashboard.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {

	// Table fields
	dataSource: FunnelDataSource;
	funnelResult: QueryResultsModel;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;
	// Selection
	selection = new SelectionModel<FunnelResultModel>(true, []);
	public config: any;

	constructor(
		private router: Router,
		private configService: LayoutConfigService,
		private dashboardService: DashboardService
	) {
		// this.subheaderService.setTitle('Dashboard');
	}

	ngOnInit(): void {
		this.dataSource = new FunnelDataSource(this.dashboardService);
		this.loadFunnelRecords();
	}

	loadFunnelRecords() {
		this.dashboardService.findFunnelRecords().subscribe(
			data => {
				console.log('httpResult : ' + JSON.stringify(data));
			},
			error => {
				console.log('httpError : ' + JSON.stringify(error));
			},
			() => {
				console.log('completed');
			}
		)
	}
	loadRecordsList1() {
		this.selection.clear();
		const queryParams = new QueryParamsModel(
			this.filterConfiguration(true),
			// this.sort.direction,
			// this.sort.active,
			// this.paginator.pageIndex,
			// this.paginator.pageSize
		);
		this.dataSource.loadRecordsList(queryParams);
		this.selection.clear();
	}


	/** FILTRATION */
	filterConfiguration(isGeneralSearch: boolean = true): any {

		const filter: any = {};
		const searchText: string = "";//this.searchInput.nativeElement.value;

		if (searchText && searchText.length > 0) {
			filter.search = searchText;
		} else {
			filter.search = '';
		}

		// if (!isGeneralSearch) {
		// 	return filter;
		// }

		return JSON.stringify(filter);
	}

}
