
export class FunnelResultModel {

    pageCount: number;
    pageNumber: number;
    pageSize: number;
    isActiveRecord: boolean;
       
    statusId: number;
    statusName: string;
    transCount: number;

}