import { Observable, of } from 'rxjs';
import { catchError, finalize, tap } from 'rxjs/operators';
import { BaseDataSource } from './_base.datasource';
import { DashboardService } from '../../../services/dashboard.service';
import { QueryParamsModel } from '../query-params.model';
import { QueryResultsModel } from '../query-results.model';


export class FunnelDataSource extends BaseDataSource {
	constructor(private dashboardService: DashboardService) {
		super();
	}

	loadRecordsList(
		queryParams: QueryParamsModel
	) {
		debugger;
		this.loadingSubject.next(true);
		this.dashboardService.findFunnelRecords().pipe(
			tap(res => {
				debugger;
				console.log(res.items);
				this.entitySubject.next(res.items);
				this.paginatorTotalSubject.next(res.totalCount);
			}),
			catchError(err => of(new QueryResultsModel([], err))),
			finalize(() => this.loadingSubject.next(false))
		).subscribe();
	}
}
