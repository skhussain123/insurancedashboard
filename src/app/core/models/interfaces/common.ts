
export interface MatItem {
    value: string;
    viewValue: string;
    cheched: boolean;
  }
  
  export interface MatSelectGroup {
    disabled?: boolean;
    name: string;
    item: MatItem[];
  }